# omwconflicts

A VFS conflict Terminal User Interface. It displays the various OpenMW VFS directories and the files which are being overridden.

It does not currently support fallback archives.

[openmw-cfg](https://gitlab.com/bmwinger/openmw-cfg) is used for getting the list of VFS directories.

## Usage

Includes an `omwconflicts` binary. If installed using the [bin/omwconflicts::openmw](https://portmod.gitlab.io/openmw-mods/bin/omwconflicts) package, run using `portmod <prefix> run omwconflicts`.

The path of openmw.cfg can be customized using the `OPENMW_CONFIG` environment variable, or `OPENMW_CONFIG_DIR`, both of which can be set in `portmod.conf` if you are running it using `portmod <prefix> run` (see [openmw-cfg](https://gitlab.com/bmwinger/openmw-cfg) for details).
