# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed
- File names are case-normalized before comparison so that files with different cases
  but which would conflict in the VFS show up.

## [0.2.0] - 2022-09-13

### Changed
- The config directory can now be set via `OPENMW_CONFIG_DIR` in addition to `OPENMW_CONFIG`
  (see https://gitlab.com/bmwinger/openmw-cfg). Both now also support tilde expansion.
