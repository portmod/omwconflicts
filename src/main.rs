mod conflicts;

use crate::conflicts::conflicts;
use openmw_cfg::{get_config, get_data_dirs};
use std::collections::{HashMap, HashSet};
use walkdir::WalkDir;

fn file_conflicts(dirs: Vec<String>, ignore: Vec<&str>) -> std::io::Result<()> {
    let mut entries = Vec::new();
    let mut ignore_set = HashSet::new();
    for extstr in ignore {
        for val in extstr.split(',') {
            ignore_set.insert(val.to_string().to_lowercase());
        }
    }
    for dir in dirs {
        let mut files = HashMap::new();
        for file in WalkDir::new(&dir)
            .into_iter()
            .filter_map(Result::ok)
            .filter(|e| {
                !e.file_type().is_dir()
                    && e.path()
                        .extension()
                        .map(|ext| !ignore_set.contains(&ext.to_str().unwrap_or("").to_lowercase()))
                        .unwrap_or(true)
            })
        {
            if let Ok(suffix) = file.path().strip_prefix(&dir) {
                files.insert(
                    suffix.display().to_string().to_lowercase(),
                    suffix.display().to_string(),
                );
            } else {
                panic!(
                    "Internal Error: File {} is not in directory {}!",
                    file.path().display(),
                    &dir
                );
            }
        }
        entries.push((dir.to_string(), files));
    }
    conflicts("Directories", "Files", &entries)?;
    Ok(())
}

fn main() -> Result<(), openmw_cfg::Error> {
    let config = get_config()?;
    file_conflicts(get_data_dirs(&config)?, vec!["txt", "md"])?;
    Ok(())
}
